# README #

### Proxy settings ###

* add to ``.profile`` file

```
export http_proxy=http://10.144.1.10:8080
export https_proxy=https://10.144.1.10:8080
```

### Celero dependencies ###

* https://github.com/DigitalInBlue/Celero
* ``sudo apt-get install libncurses5-dev``

#### apt-get + proxy ####

* ``sudo nano /etc/apt/apt.conf``
* add the folllowing lines:

```
Acquire::http::Proxy "http://10.144.1.10:8080";
Acquire::https::Proxy "https://10.144.1.10:8080";

```

### ANKIETA ###

* https://docs.google.com/forms/d/e/1FAIpQLSf3yMWdtmS247kQkMGJCtLUtzgd6Vt6prxpTHeEIu3TDqA9Og/viewform?hl=pl
