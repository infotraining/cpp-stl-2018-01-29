#define CATCH_CONFIG_MAIN

#include <array>
#include <iostream>
#include <string>
#include <vector>
#include <deque>
#include <list>

#include "catch.hpp"

using namespace std;

namespace Explain
{
    struct Aggregate
    {
        int a;
        double b;
        int tab[3];
    };

    template <typename T, size_t N>
    struct Array
    {
        T items[N];

        // no constructor

        using iterator       = T*;
        using const_iterator = const T*;

        constexpr size_t size() const
        {
            return N;
        }

        constexpr iterator begin()
        {
            return items;
        }

        constexpr iterator end()
        {
            return items + N;
        }
    };
};

TEST_CASE("Aggregates")
{
    SECTION("can be initilized with {}")
    {
        Explain::Aggregate agg{1, 3.14, {1, 2, 3}};

        REQUIRE(agg.a == 1);
        REQUIRE(agg.b == Approx(3.14));

        SECTION("zero-init")
        {
            Explain::Aggregate agg2{};
            REQUIRE(agg2.a == 0);
            REQUIRE(agg2.b == 0.0);
        }
    }
}

template <typename Container>
void print(const Container& container, const std::string& prefix)
{
    cout << prefix << ": [ ";
    for (const auto& item : container)
        cout << item << " ";
    cout << "]" << endl;
}

TEST_CASE("std::array")
{
    SECTION("is substitute for static array")
    {
        int tab[10] = {1, 2, 3, 4};
        print(tab, "tab");

        array<int, 10> arr = {{1, 2, 3, 4}};
        print(arr, "arr");

        SECTION("default init")
        {
            array<int, 10> arr_default; // uninitzialized
            array<int, 10> arr_default2{}; // all items are zeroed
        }
    }

    SECTION("size() and indexing")
    {
        array<int, 256> buffer = {};

        array<int, buffer.size()> backup = {};

        for (size_t i = 0; i < buffer.size(); ++i)
            buffer[i] = i * 2;

        SECTION("at()")
        {
            REQUIRE_THROWS_AS(buffer.at(1024), std::out_of_range);
        }
    }

    SECTION("iterators")
    {
        array<int, 256> buffer = {};

        array<int, 256>::iterator it = buffer.begin();
    }

    SECTION("swapping")
    {
        array<int, 4> arr1 = {1, 2, 3, 4};
        array<int, 4> arr2 = {-1, 5, 3, 5};

        print(arr1, "arr1");
        print(arr2, "arr2");

        arr1.swap(arr2);
        cout << "after swap\n";

        print(arr1, "arr1");
        print(arr2, "arr2");
    }
}

namespace LegacyCode
{
    void use_tab(int* tab, size_t size)
    {
        cout << "using tab: ";
        for (int* it = tab; it != tab + size; ++it)
            cout << *it << " ";
        cout << "\n";
    }
}

TEST_CASE("array - working with legacy code")
{
    array<int, 10> arr = {1, 2, 3, 4};
    LegacyCode::use_tab(arr.data(), arr.size());
}

auto get_point()
{
    return array{1, 2, 3};
}

TEST_CASE("array - Cpp17 with structure bindings")
{
    auto[x, y, z] = get_point();

    REQUIRE(x == 1);
    REQUIRE(y == 2);
    REQUIRE(z == 3);
}

class LegacyGadget
{
    int id_;
    int* data_;
    size_t size_;
public:
    LegacyGadget(int id, size_t size) : id_{id}, data_{new int[size]}, size_{size}
    {}

    ~LegacyGadget()
    {
        delete[] data_;
    }

    LegacyGadget(const LegacyGadget& source)
        : id_{source.id_}, data_{new int[source.size_]}, size_{source.size_}
    {
        // copy of data
    }

    void swap(LegacyGadget& other) noexcept
    {
        std::swap(id_, other.id_);
        std::swap(data_, other.data_);
        std::swap(size_, other.size_);
    }

    LegacyGadget& operator=(const LegacyGadget& source)
    {
        if (this != &source)
        {
            LegacyGadget temp(source);
            swap(temp);
        }

        return *this;
    }
};

class Gadget
{
    int id_;
    string name_;
public:
    static int gen_id()
    {
        static int id_seed;
        return ++id_seed;
    }

    Gadget() : id_{gen_id()}, name_{"unknown"}
    {
        cout << "Gadget(" << id_ << ", " << name_ << ")" << endl;
    }

    Gadget(int id, string name = "unknown") : id_{id}, name_{name}
    {
        cout << "Gadget(" << id_ << ", " << name_ << ")" << endl;
    }

    Gadget(const Gadget& source) : id_{source.id_}, name_{source.name_}
    {
        cout << "Gadget(cc: " << id_ << ", " << name_ << ")" << endl;
    }

    Gadget(Gadget&& source) noexcept : id_{source.id_}, name_{std::move(source.name_)}
    {
        cout << "Gadget(mv: " << id_ << ", " << name_ << ")" << endl;
    }

    Gadget& operator=(const Gadget& source)
    {
        if (this != &source)
        {
            id_ = source.id_;
            name_ = source.name_;
        }

        return *this;
    }

    Gadget& operator=(Gadget&& source)
    {
        if (this != &source)
        {
            id_ = source.id_;
            name_ = std::move(source.name_);
        }

        return *this;
    }

    int id() const
    {
        return id_;
    }

    string name() const
    {
        return name_;
    }
};

ostream& operator<<(ostream& out, const Gadget& g)
{
    out << "Gadget{id: " << g.id() << ", name: " << g.name() << "}";
    return out;
}

TEST_CASE("vector - constructor")
{
    SECTION("default contruction")
    {
        vector<int> vec;

        REQUIRE(vec.size() == 0);
        REQUIRE(vec.empty());
        REQUIRE(vec.capacity() == 0);

        print(vec, "vec");
    }

    SECTION("constructor with size as argument")
    {
        vector<int> vec(10);

        REQUIRE(vec.size() == 10);
        REQUIRE(vec.capacity() >= 10);
        print(vec, "vec");

        vector<Gadget> gadgets(10);
        print(gadgets, "gadgets");

        SECTION("constructor with size and value")
        {
            vector<Gadget> gadgets2(10, Gadget{1, "ipad"});
            print(gadgets2, "gadgets2");
        }
    }

    SECTION("constructor with iterators")
    {
        array<int, 10> numbers = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
        vector<int> vec(numbers.begin(), numbers.end());
        print(vec, "vec");

        int tab[10] = { 1, 2, 3, 4 };
        vector<int> vec2(begin(tab), end(tab));
        print(vec2, "vec2");
    }

    SECTION("constructor with initializer list")
    {
        vector<int> vec = { 1, 2, 3, 4 }; // vector<int>(initilizer_list<int>)
        print(vec, "vec");

        vector<int> vec1(10, 2); // {2, 2, 2, 2, 2, 2, 2, 2, 2, 2 }
        vector<int> vec2{10, 2}; // {10, 2}
    }
}

TEST_CASE("initializer_list")
{
    initializer_list<int> il1{1, 2, 3, 4};

    REQUIRE(il1.size() == 4);

    for(auto it = il1.begin(); it != il1.end(); ++it)
        cout << (*it) << " ";
    cout << "\n";

    auto il2 = {1, 2, 3, 4}; // initializer_list<int>

    for(const auto& item : { 1, 2, 3 })
    {
        cout << item << " ";
    }
    cout << endl;
}

TEST_CASE("vector - inserting items")
{
    vector<Gadget> gadgets;

    Gadget g{1, "ipad"};
    gadgets.push_back(g); // inserts copy of g into container // vector<Gadget>::push_back(const Gadget&)
    gadgets.push_back(Gadget{2, "ipod"}); // vector<Gadget>::push_back(Gadget&&)
    gadgets.emplace_back(3, "icos"); // template<Args...> vector<Gadget>::emplace_back(Args&&... args)

    auto pos = gadgets.begin() + 1;
    pos = gadgets.insert(pos, Gadget{4, "ipad2"});
    gadgets.emplace(pos, 5, "iphone");

    print(gadgets, "gadgets after inserts");
}


TEST_CASE("vector - optimizing efficiency")
{
    cout << "\n\n";

    vector<Gadget> gadgets;

    auto show_stats = [](const auto& vec) { cout << "size: " << vec.size() << " - capacity: " << vec.capacity() << endl; };

    for(int i = 1; i <= 32; ++i)
    {
        gadgets.push_back(Gadget{i, "g" + to_string(i)});
        show_stats(gadgets);
    }
}

class X
{
public:
    int a = 8;

    X() = default;

    X(int a) : a{a}
    {}

    X(const X&) = default;
    X& operator=(const X&) = default;
    X(X&&) = default;
    X& operator=(X&&) = default;

    virtual ~X() = default;
};

TEST_CASE("X")
{
    X x;

    REQUIRE(x.a == 8);
}

TEST_CASE("vector - erase")
{
    vector<int> vec = { 1, 2, 3, 4 };

    auto pos = vec.begin() + 3;

    SECTION("inefficient")
    {
        vec.erase(vec.begin() + 2);
    }

    SECTION("efficient")
    {
        std::swap(*pos, vec.back());
        vec.pop_back();
    }

    print(vec, "vec after erase");

}

TEST_CASE("vector<bool>")
{
    vector<bool> vec_bool = { 1, 0, 0, 1, 1, 0 };

    for(auto&& bit : vec_bool)
        bit.flip();

    print(vec_bool, "vec_bool");
}

TEST_CASE("deque")
{
    deque<int> dq = { 1, 2, 3 };

    dq.push_front(0);
    dq.push_back(4);

    print(dq, "dq");

    dq.pop_front();

    print(dq, "dq");
}

TEST_CASE("list")
{
    list<int> lst = { 1, 6, 23, 12, 12, 4554, 6, 665, 33, 77 };

    SECTION("push_back, push_front & insert are efficient")
    {
        lst.push_back(9);
        lst.push_front(10);

        auto pos = lst.begin();
        ++pos;

        lst.insert(pos, 13);

        print(lst, "lst");
    }

    SECTION("special ops")
    {
        lst.sort();

        list<int> lst2 = { 1, 4, 6};

        lst.merge(lst2);

        cout << "after merge" << endl;
        print(lst, "lst");
        print(lst2, "lst2");

        lst.reverse();
        lst.unique();

        print(lst, "lst");
    }

    SECTION("splicing")
    {
        auto pos1 = ++(lst.begin());
        auto pos2 = --(lst.end());

        list<int> lst2 = { 1001, 1002, 1003 };

        auto where = ++(lst2.begin());

        cout << "Before splice\n";
        print(lst, "lst");
        print(lst2, "lst2");

        lst.splice(where, lst, pos1, pos2);

        cout << "\nAfter splice\n";
        print(lst, "lst");
        print(lst2, "lst2");
    }
}
