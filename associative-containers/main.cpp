﻿#define CATCH_CONFIG_MAIN

#include <boost/functional/hash.hpp>
#include <iostream>
#include <random>
#include <set>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include "catch.hpp"

using namespace std;

template <typename Container>
void print(const Container& container, const string& prefix = "data")
{
    cout << prefix << ": [ ";
    for (const auto& item : container)
    {
        cout << item << " ";
    }
    cout << "]" << endl;
}

template <typename T1, typename T2>
ostream& operator<<(ostream& out, const pair<T1, T2>& p)
{
    out << "{" << p.first << ", " << p.second << "}";
    return out;
}

TEST_CASE("set")
{
    set<int> set_int = {1, 7, 34, 665, 42, 3, 3, 7};

    print(set_int, "set_int");

    SECTION("insert")
    {
        set<int>::iterator pos;
        bool insert_performed;
        tie(pos, insert_performed) = set_int.insert(13);

        REQUIRE(*pos == 13);
        REQUIRE(insert_performed == true);

        SECTION("since C++17 - structured bindings")
        {
            auto[pos, insert_performed] = set_int.insert(13);
            REQUIRE(insert_performed == false);
        }
    }

    SECTION("finding a key")
    {
        auto pos = set_int.find(665);
        if (pos != set_int.end())
        {
            cout << "Item found: " << *pos << endl;
        }

        SECTION("since C++17")
        {
            if (auto pos = set_int.find(665); pos != set_int.end())
            {
                cout << "Item found: " << *pos << endl;
            }
        }
    }

    SECTION("counting")
    {
        if (set_int.count(665))
        {
            cout << "Item is in the set" << endl;
        }
    }

    SECTION("erasing a key")
    {
        set_int.erase(665);

        print(set_int, "set_int");
    }
}

TEST_CASE("multiset")
{
    multiset<int> mset_int = {5, 234, 665, 42, 42, 8, 13, 13, 665, 42};
    print(mset_int, "mset_int");

    SECTION("lower_bound & upper_bound")
    {
        auto it1 = mset_int.lower_bound(42);
        auto it2 = mset_int.upper_bound(42);

        REQUIRE(distance(it1, it2) == 3);

        for (auto it = it1; it != it2; ++it)
            cout << *it << " ";
        cout << endl;

        tie(it1, it2) = mset_int.equal_range(17);

        if (it1 == it2)
        {
            cout << "Item not present in multiset" << endl;
            mset_int.insert(it1, 17);
        }

        print(mset_int, "mset");
    }
}

TEST_CASE("custom comparers")
{
    SECTION("descending order")
    {
        multiset<int, greater<int>> mset_desc = {5, 234, 5, 12, 66, 42, 42, 665, 5};
        print(mset_desc, "mset_desc");
    }

    SECTION("lambda expression")
    {
        multiset<int, greater<int>> mset_desc = {5, 234, 5, 12, 66, 42, 42, 665, 5};

        auto comp_ptrs = [](const auto* a, const auto* b) { return *a < *b; };

        multiset<const int*, decltype(comp_ptrs)> mset_ptrs(comp_ptrs);

        for (auto& item : mset_desc)
            mset_ptrs.insert(&item);

        // printing
        cout << "mset_ptrs: ";
        for (const auto& ptr : mset_ptrs)
            cout << *ptr << " ";
        cout << endl;
    }
}

template <typename T>
struct Less
{
    bool operator()(const T& a, const T& b) const
    {
        return a < b;
    }
};

template <>
struct Less<void>
{
    using is_transparent = std::true_type;

    template <typename T, typename U>
    bool operator()(T&& t, U&& u) const
    {
        return std::forward<T>(t) < std::forward<U>(u);
    }
};

struct TransparentLess
{
    using is_transparent = std::true_type;

    template <typename T, typename U>
    bool operator()(T&& t, U&& u) const
    {
        return std::forward<T>(t) < std::forward<U>(u);
    }
};

TEST_CASE("transparent comparers")
{
    //set<string, TransparentLess> words = { "one", "two", "three" };
    set<string, less<>> words = {"one", "two", "three"};

    auto pos = words.find("two");

    REQUIRE(*pos == "two"s);
}

TEST_CASE("maps")
{
    map<int, string> dict = {{5, "five"}, {1, "one"}, {2, "two"}, {3, "three"}};

    print(dict, "dict");

    SECTION("inserting")
    {
        dict.insert(make_pair(6, "six"));
        dict.insert(decltype(dict)::value_type{7, "seven"});
        dict.emplace(8, "eight");

        print(dict, "dict");

        auto result = dict.insert(make_pair(5, "FIVE"));
        REQUIRE(result.second == false);
        REQUIRE(dict[5] == "five");
    }

    SECTION("indexing")
    {
        auto& mapped_value = dict[1];

        REQUIRE(mapped_value == "one");

        mapped_value = "ONE";

        SECTION("when key does not exist")
        {
            dict[10];

            print(dict, "dict");

            SECTION("at() throws")
            {
                REQUIRE_THROWS_AS(dict.at(11), std::out_of_range);
            }
        }
    }

    SECTION("range based for")
    {
        vector<string> words = {"one", "two"};

        for (const auto& item : words)
        {
            cout << item;
        }

        SECTION("complier works like this")
        {
            for (auto it = words.begin(); it != words.end(); ++it)
            {
                const auto& item = *it;
                cout << item;
            }
        }
    }

    SECTION("iterating over map")
    {
        for (const auto& kv : dict)
            cout << kv.first << " - " << kv.second << endl;

        SECTION("since C++17")
        {
            for (const auto & [ key, value ] : dict)
                cout << key << " - " << value << endl;
        }
    }
}

template <typename HashContainer>
void print_stats(const HashContainer& c, const string& prefix)
{
    cout << prefix << ":"
         << " size: " << c.size()
         << "; bucket_count: " << c.bucket_count()
         << "; max_load_factor: " << c.max_load_factor()
         << "; load_factor: " << c.load_factor() << endl;
}

TEST_CASE("unordered_set")
{
    unordered_set<int> uset_int = {1, 534, 665, 4, 34, 234, 55, 66, 234, 66};

    print(uset_int, "uset_int");

    SECTION("find, count, insert,& erase are O(1)")
    {
        uset_int.insert(9);

        REQUIRE(uset_int.count(9) == 1);
    }
}

TEST_CASE("unordered_set - rehashing")
{
    unordered_set<int> uset_int;

    //uset_int.rehash(128);
    uset_int.max_load_factor(0.3);

    cout << "\n\n";
    print_stats(uset_int, "uset_int");

    random_device rd;
    mt19937_64 rnd_gen(rd());
    uniform_int_distribution<> rnd_distr(1, 1000);

    for (int i = 0; i < 200; ++i)
    {
        uset_int.insert(rnd_distr(rnd_gen));
        print_stats(uset_int, "uset_int");
    }
}

class Gadget
{
private:
    int id_;
    string name_;

    auto tied() const
    {
        return tie(id_, name_);
    }

public:
    static int gen_id()
    {
        static int id_seed;
        return ++id_seed;
    }

    Gadget()
        : id_{gen_id()}
        , name_{"unknown"}
    {
    }

    Gadget(int id, string name = "unknown")
        : id_{id}
        , name_{name}
    {
    }

    int id() const
    {
        return id_;
    }

    string name() const
    {
        return name_;
    }

    bool operator==(const Gadget& other) const
    {
        return tied() == other.tied();
    }

    bool operator<(const Gadget& other) const
    {
        return tied() < other.tied();
    }
};

ostream& operator<<(ostream& out, const Gadget& g)
{
    out << "Gadget{id: " << g.id() << ", name: " << g.name() << "}";
    return out;
}

namespace Cpp17
{
    template <typename... Args>
    size_t get_hash_combine(Args&&... args)
    {
        size_t seed{};

        (boost::hash_combine(seed, std::forward<Args>(args)), ...);

        return seed;
    }
}

struct HashGadget
{
    size_t operator()(const Gadget& g) const
    {
        size_t seed{};
        boost::hash_combine(seed, g.id());
        boost::hash_combine(seed, g.name());

        return seed;

        // alt
        //return Cpp17::get_hash_combine(g.id(), g.name());
    }
};

namespace std
{
    template <>
    struct hash<Gadget>
    {
        size_t operator()(const Gadget& g) const
        {
            size_t h_id   = std::hash<int>{}(g.id());
            size_t h_name = std::hash<string>{}(g.name());

            return h_id ^ (h_name << 1);
        }
    };
}

TEST_CASE("custom hash")
{
    unordered_set<Gadget, HashGadget> gadgets;
    gadgets.emplace(1, "ipad");
    gadgets.emplace(2, "ipod");
    print(gadgets, "gadgets");


    unordered_set<Gadget> gadgets_alt;
}
