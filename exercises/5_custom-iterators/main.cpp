#define CATCH_CONFIG_MAIN

#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;
using namespace Catch::Matchers;

class RangeIterator
{
    int value_;
public:
    explicit RangeIterator(int value) : value_{value}
    {}

    RangeIterator& operator++() // prefix: ++it
    {
        ++value_;
        return *this;
    }

    RangeIterator operator++(int) // postfix: it++
    {
        RangeIterator snapshot{*this};
        ++value_;
        return snapshot;
    }

    bool operator!=(const RangeIterator& other) const
    {
        return value_ != other.value_;
    }

    bool operator==(const RangeIterator& other) const
    {
        return value_ == other.value_;
    }

    int operator*() const
    {
        return value_;
    }
};

class Range
{
    int start_{};
    int end_{};
public:
    using iterator = RangeIterator;

    Range(int start, int end) : start_{start}, end_{end}
    {}

    iterator begin() const
    {
        return RangeIterator{start_};
    }

    iterator end() const
    {
        return RangeIterator{end_};
    }
};

TEST_CASE("Range")
{
    vector<int> data;

    SECTION("empty range")
    {
        for(const auto& item : Range{1, 1})
        {
            data.push_back(item);
        }

        SECTION("is interpreted as")
        {
            auto range = Range{1, 1};

            for(auto it = range.begin(); it != range.end(); ++it)
            {
                const auto& item = *it;
                data.push_back(item);
            }
        }

        REQUIRE(data.empty()); 
    }

    SECTION("many items")
    {
        for(const auto& item : Range{1, 10})
        {
            data.push_back(item);
        }

        REQUIRE_THAT(data, Equals(vector<int>{1, 2, 3, 4, 5, 6, 7, 8, 9})); 
    }
}
