#include <algorithm>
#include <cmath>
#include <functional>
#include <iostream>
#include <iterator>
#include <numeric>
#include <random>
#include <set>
#include <vector>

using namespace std;

template <typename Container>
void print(const Container& container, const string& prefix = "items")
{
    cout << prefix << " : [ ";
    for (const auto& item : container)
        cout << item << " ";
    cout << "]" << endl;
}

int main()
{
    vector<int> vec(35);

    std::random_device rd;
    std::mt19937 rnd_engine{rd()};
    std::uniform_int_distribution<int> uniform_distr{1, 35};

    auto rnd_gen = [&uniform_distr, &rnd_engine] { return uniform_distr(rnd_engine); };

    generate(vec.begin(), vec.end(), rnd_gen);

    vector<int> sorted_vec{vec};
    sort(sorted_vec.begin(), sorted_vec.end());

    multiset<int> mset{vec.begin(), vec.end()};

    print(vec, "vec        : ");
    print(sorted_vec, "sorted_vec : ");
    print(mset, "mset       : ");

    // check if 17 is in sequence

    // print number of occurence of 22

    // remove all numbers greater than 15
}
