#include <boost/algorithm/string.hpp>
#include <chrono>
#include <fstream>
#include <iostream>
#include <set>
#include <string>
#include <vector>
#include <unordered_set>
#include <iterator>

using namespace std;

int main()
{
    // wszytaj zawartość pliku en.dict ("słownik języka angielskiego")
    // sprawdź poprawość pisowni następującego zdania:    
    auto words_list = { "this", "is", "an", "exmple", "of", "very", "badd", "snetence" };

    vector<string> misspelled;
    misspelled.reserve(100);

    ifstream file_in("en.dict");

    if (!file_in)
    {
        cerr << "Error opening a file!" << endl;
        return -1;
    }

    auto t_start = chrono::high_resolution_clock::now();

    istream_iterator<string> file_start(file_in);
    istream_iterator<string> file_end;

    unordered_set<string> dictionary(file_start, file_end, 520241);
    dictionary.max_load_factor(0.3);

    for(const auto& word : words_list)
    {
        if (not dictionary.count(word))
            misspelled.push_back(word);
    }

    auto t_stop = chrono::high_resolution_clock::now();

    cout << "Misspelled words: ";
    for(const auto& word : misspelled)
        cout << word << " ";
    cout << endl;

    cout << "Elapsed time: " << chrono::duration_cast<chrono::milliseconds>(t_stop - t_start).count() << endl;
}
