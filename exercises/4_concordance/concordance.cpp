#include <algorithm>
#include <cctype>
#include <chrono>
#include <fstream>
#include <iostream>
#include <iterator>
#include <map>
#include <set>
#include <string>
#include <unordered_map>
#include <vector>
#include <boost/bimap.hpp>
#include <boost/bimap/unordered_set_of.hpp>
#include <boost/bimap/multiset_of.hpp>
#include <unordered_map>

using namespace std;

/*
    Napisz program zliczający ilosc wystapien danego slowa w pliku tekstowym. Wyswietl 20 najczęściej występujących slow (w kolejności malejącej).
*/

string str_trim(const std::string& txt)
{
    auto it_first_c = find_if(begin(txt), end(txt), [](auto c) { return std::isalpha(c); });
    auto it_last_c  = find_if(rbegin(txt), make_reverse_iterator(it_first_c), [](auto c) { return std::isalpha(c); }).base();

    return string(it_first_c, it_last_c);
}

string str_to_lower(const std::string& txt)
{
    string result(txt.size(), '\0');

    transform(txt.begin(), txt.end(), result.begin(), [](auto c) { return std::tolower(c); });

    return result;
}

namespace Experimantal
{
    template <typename Iter, typename F>
    void for_each_n(Iter it, size_t n, F f)
    {
        for (size_t i = 0; i < n; ++i)
        {
            f(*it);
            ++it;
        }
    }
}

void impl1(const vector<string>& words)
{
    auto t1 = chrono::high_resolution_clock::now();

    // making a concordance
    map<string, size_t> word_counter;

    for (const auto& word : words)
        word_counter[word]++;

    using word_counter_pair = decltype(word_counter)::value_type;

    auto comp_by_count = [](const auto& p1, const auto& p2) { return p1->second > p2->second; };
    set<word_counter_pair*, decltype(comp_by_count)> concordance(comp_by_count);

    for (auto& p : word_counter)
        concordance.insert(&p);

    auto t2 = chrono::high_resolution_clock::now();

    cout << "\nResult:\n";
    Experimantal::for_each_n(concordance.begin(), 20,
                             [](const auto& p) { cout << p->first << " - " << p->second << endl; });

    cout << "\nElapsed time: " << chrono::duration_cast<chrono::milliseconds>(t2 - t1).count() << endl;
}

void impl2(const vector<string>& words)
{
    auto t1 = chrono::high_resolution_clock::now();

    // making a concordance
    unordered_map<string, size_t> word_counter(26267);

    for (const auto& word : words)
        word_counter[word]++;

    using word_counter_pair = decltype(word_counter)::value_type;

    auto comp_by_count = [](const auto& p1, const auto& p2) { return p1->second > p2->second; };
    set<word_counter_pair*, decltype(comp_by_count)> concordance(comp_by_count);

    for (auto& p : word_counter)
        concordance.insert(&p);

    auto t2 = chrono::high_resolution_clock::now();

    cout << "\nResult:\n";
    Experimantal::for_each_n(concordance.begin(), 20,
                             [](const auto& p) { cout << p->first << " - " << p->second << endl; });

    cout << "\nElapsed time: " << chrono::duration_cast<chrono::milliseconds>(t2 - t1).count() << endl;
}


int main()
{
    const string file_name = "proust.txt";

    ifstream fin(file_name);

    if (!fin)
        throw runtime_error("File "s + file_name + " can't be opened");

    vector<string> words;
    words.reserve(200'000);

    string token;

    while (fin >> token)
    {
        string word = str_trim(token);

        if (word.length() > 0)
            words.push_back(str_to_lower(word));
    }

    cout << "Words: " << words.size() << endl;

    impl1(words);

    impl2(words);
}
