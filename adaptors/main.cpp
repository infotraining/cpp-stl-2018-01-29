#define CATCH_CONFIG_MAIN

#include <iostream>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <bitset>

#include "catch.hpp"

using namespace std;

TEST_CASE("stack")
{
    vector<string> buffer;
    buffer.reserve(1000);
    stack<string, vector<string>> stck(move(buffer));

    SECTION("pushing")
    {
        for(int i = 1; i <= 10; ++i)
            stck.push(to_string(i));
    }

    SECTION("popping from stack - other thread")
    {
        while(not stck.empty())
        {
            string item = stck.top();
            stck.pop();
        }
    }
}

TEST_CASE("queue")
{
    queue<string> q;

    SECTION("pushing")
    {
        for(int i = 1; i <= 10; ++i)
            q.push(to_string(i));
    }

    SECTION("popping from stack - other thread")
    {
        while(not q.empty())
        {
            string item = q.front();
            q.pop();
        }
    }
}

TEST_CASE("bitset")
{
    bitset<16> bs = 0b0000'01000; //bitset<16> bs(8);

    cout << "bs: " << bs << endl;

    bs.flip();
    bs.flip(0);

    bs = bs << 2;

    cout << "bs: " << bs << " - " << bs.to_ulong() << endl;

    bitset<16> bs2{"010101010"};

    cout << "bs2: " << bs2 << endl;

    cout << "(bs | ~(bs2 >> 1)) - " << (bs | ~(bs2 >> 1)) << endl;

    REQUIRE(bs2.test(1));

    bs2[1] = 0;

    REQUIRE_FALSE(bs2.test(1));
}
