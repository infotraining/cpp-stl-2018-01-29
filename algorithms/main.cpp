#define CATCH_CONFIG_MAIN

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <numeric>
#include <random>
#include <iterator>

#include "catch.hpp"

using namespace std;

template <typename Container>
void print(const Container& container, const string& prefix = "data")
{
    cout << prefix << ": [ ";
    for (const auto& item : container)
    {
        cout << item << " ";
    }
    cout << "]" << endl;
}

TEST_CASE("algorithms")
{
    vector<int> vec(20);

    iota(vec.begin(), vec.end(), 1);

    mt19937_64 rnd_gen(665);
    shuffle(vec.begin(), vec.end(), rnd_gen);

    print(vec, "vec");

    SECTION("counting")
    {
        auto result = count_if(vec.begin(), vec.end(), [](int x) { return x > 10; });

        REQUIRE(result == 10);
    }

    SECTION("all_of; any_of; none_of")
    {
        REQUIRE(none_of(vec.begin(), vec.end(), [](int x) { return x < 0; }));
        REQUIRE(all_of(vec.begin(), vec.end(), [](int x) { return x > 0; }));
    }

    SECTION("min_element, max_element")
    {
        auto minmax = minmax_element(vec.begin(), vec.end());

        REQUIRE(*minmax.first == 1);
        REQUIRE(*minmax.second == 20);
    }

    SECTION("find")
    {
        auto pos = find_if(vec.begin(), vec.end(), [](int x) { return x % 2 == 0; });

        if (pos != vec.end())
            cout << "Even is found: " << *pos << endl;

        auto sq = { 15, 12, 6 };

        auto sq_start = find_end(vec.begin(), vec.end(), sq.begin(), sq.end());

        cout << *sq_start << endl;
    }

    SECTION("searching in sorted container")
    {
        vec.insert(vec.end(), { 5, 6, 6, 6, 23, 45, 42, 5, 6 });

        sort(vec.begin(), vec.end());

        REQUIRE(binary_search(vec.begin(), vec.end(), 42));

        auto sq_6 = equal_range(vec.begin(), vec.end(), 6);

        REQUIRE(distance(sq_6.first, sq_6.second) == 5);
    }

    SECTION("generate")
    {
        auto gen_pow_2 = [seed = 1]() mutable { seed *= 2; return seed; };

        vector<int> vec(10);

        generate(vec.begin(), vec.end(), gen_pow_2);

        print(vec, "vec");
    }

    SECTION("remove")
    {
        print(vec, "vec before remove");

        auto real_remove_if = [&vec](auto pred) {
            auto new_end =  remove_if(vec.begin(), vec.end(), pred);
            vec.erase(new_end, vec.end());
        };

        real_remove_if([](int x) { return x > 10;});

        print(vec, "vec after remove");
    }

    SECTION("n-th element")
    {
        nth_element(vec.begin(), vec.begin() + 5, vec.end(), greater<>{});

        cout << "5 the biggest numbers: ";
        copy_n(vec.begin(), 5, ostream_iterator<int>(cout, " "));
        cout << endl;
    }

    SECTION("partial sort")
    {
        partial_sort(vec.begin(), vec.begin() + 5, vec.end(), greater<>{});

        cout << "5 the biggest numbers: ";
        copy_n(vec.begin(), 5, ostream_iterator<int>(cout, " "));
        cout << endl;
    }

    SECTION("set operations")
    {
        vector<int> vec1 = { 1, 2, 3, 4, 5, 6 };
        vector<int> vec2 = { 4, 5, 6, 7, 8, 9, 10 };

        vector<int> intersection_vec;

        set_intersection(vec1.begin(), vec1.end(), vec2.begin(), vec2.end(),
                         back_inserter(intersection_vec));

        print(intersection_vec, "intersection_vec");
    }
}
