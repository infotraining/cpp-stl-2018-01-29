#include <iostream>
#include <string>
#include <vector>
#include <iterator>
#include <algorithm>

using namespace std;

int main()
{
    istream_iterator<string> start(cin);
    istream_iterator<string> end;

    cout << "Podaj dane:" << endl;

    vector<string> vec(start, end);

    cout << "\nvec: ";
    copy(vec.begin(), vec.end(), ostream_iterator<string>(cout, " "));
    cout << endl;
}
