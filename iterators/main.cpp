#define CATCH_CONFIG_MAIN

#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <algorithm>

#include "catch.hpp"

using namespace std;

template <typename Container>
void print(const Container& container, const string& prefix = "data")
{
    cout << prefix << ": [ ";
    for (const auto& item : container)
    {
        cout << item << " ";
    }
    cout << "]" << endl;
}

namespace Explain
{
    template <typename InpIter, typename OutIter>
    OutIter copy(InpIter first, InpIter last, OutIter result)
    {
        for(auto it = first; it != last; ++it)
        {
            *result = *it;
            ++result;
        }

        return result;
    }

    template <typename InpIter, typename OutIter, typename Pred>
    OutIter copy(InpIter first, InpIter last, OutIter result, Pred pred)
    {
        for(auto it = first; it != last; ++it)
        {
            if (pred(*it))
                *result = *it;
            ++result;
        }

        return result;
    }

    template<typename RndIt>
    void print_sample(RndIt first, RndIt last, size_t count)
    {
        for(auto it = first; it < last; it += count)
        {
            cout << *it << " ";
        }
        cout << endl;
    }
}


TEST_CASE("iterators")
{
    vector<int> vec = { 1, 2, 3, 4, 5, 42, 7 };

    list<int> lst(vec.size());

    Explain::copy(vec.rbegin(), vec.rend(), lst.begin());

    cout << "item in the middle: " << (*next(lst.begin(), lst.size()/2)) << endl;

    sort(vec.begin(), vec.end(), greater<>());

    Explain::print_sample(vec.begin(), vec.end(), 2);
}

TEST_CASE("insert iterators")
{
    vector<int> vec = { 1, 2, 3, 4, 5, 42, 7 };

    vector<int> evens;

    //back_insert_iterator<vector<int>> back_it(evens);

    copy_if(vec.begin(), vec.end(), back_inserter(evens), [](int x) { return x % 2 == 0; });

    print(evens, "evens");

    vector<int> data = {665, 667};

    copy_if(vec.begin(), vec.end(), inserter(data, data.begin() + 1), [](int x) { return x % 2 != 0; });

    print(data, "data");
}

TEST_CASE("move iterators")
{
    vector<string> words = { "one", "two", "three" };
    vector<string> short_words;

    copy_if(make_move_iterator(words.begin()), make_move_iterator(words.end()),
            back_inserter(short_words),
            [](const auto& str) { return str.length() <= 3; });

    print(words, "words");
    print(short_words, "short_words");
}


template <typename Iter1, typename Iter2>
auto zip(Iter1 first1, Iter1 last1, Iter2 first2)
{
    using T1 = decay_t<decltype(*first1)>;
    using T2 = typename iterator_traits<Iter2>::value_type;
    using ZippedPair = tuple<T1, T2>;

    vector<ZippedPair> zipped_result;

    while(first1 != last1)
    {
        zipped_result.emplace_back(*first1, *first2);
        ++first1;
        ++first2;
    }

    return zipped_result;
}

TEST_CASE("zip")
{
    const vector<int> numbers = { 1, 2, 3 };
    string words[] = { "one", "two", "three" };

    auto zipped = zip(numbers.begin(), numbers.end(), begin(words));

    REQUIRE(zipped[0] == make_tuple(1, "one"));
    REQUIRE(zipped[1] == make_tuple(2, "two"));
}

template <typename T>
void AUTO1(T obj)
{
}

template <typename T>
void AUTO2(T& obj)
{
}

template <typename T>
void AUTO3(T&& obj)
{
}

TEST_CASE("auto vs. decltype vs. decltype(auto)")
{
    SECTION("auto")
    {
        int x = 10;
        int& ref_x = x;
        const int& cref_x = x;

        auto ax1 = x; // int
        auto ax2 = ref_x; // int
        auto ax3 = cref_x; // int

        AUTO1(x);

        auto& arx1 = x; // int&
        auto& arx2 = ref_x; // int&
        auto& arx3 = cref_x; //const int&

        AUTO2(cref_x);

        auto&& urx1 = 10; // int&&
        auto&& urx2 = x;  // int&
        auto&& urx3 = ref_x; // int&
        auto&& urx4 = cref_x; // const int&

        auto il = { 1, 2, 3}; // initializer_list<int>
    }

    SECTION("decltype")
    {
        vector<int> v1 = { 1, 2, 3, 4 };
        decltype(v1) v2; // vector<int>
        auto v3 = v1; // vector<int> + copy constructor

        decltype(v1[0]) x = v1[0]; // int&
        auto& y = v1[0]; // int&
    }
}


// C++14
template <typename F>
decltype(auto) call(F f)
{
    return f();
}

int tab[3] = { 1, 2, 3 };

int& get_first()
{
    return tab[0];
}

struct MAGIC_LAMBDA_6423784528375
{
    decltype(auto) operator()() const
    {
        return tab[0];
    }
};

TEST_CASE("calling")
{
    SECTION("function")
    {
        auto&& result = call(&get_first);

        static_assert(is_same<decltype(result), int&>::value, "Error");
    }

    SECTION("lambda")
    {
        auto lambda_get_first = []() -> decltype(auto) { return tab[0]; };

        auto&& result = call(lambda_get_first);

        static_assert(is_same<decltype(result), int&>::value, "Error");
    }
}
